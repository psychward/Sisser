package com.psychward.sisser;

import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.monster.Creeper;
import net.minecraft.world.level.Explosion;
import net.minecraft.world.level.Level;
import net.minecraftforge.event.level.ExplosionEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.List;

@Mod.EventBusSubscriber
public class SisserHandler {

    @SubscribeEvent
    public static void onDetonate(ExplosionEvent.Detonate event) {
        Level world = event.getLevel();
        if (world.isClientSide()) {
            return;
        }
        Explosion explosion = event.getExplosion();
        if (explosion.getExploder() instanceof Creeper) {
            List<BlockPos> blocks = event.getAffectedBlocks();
            List<Entity> entities = event.getAffectedEntities();
            if (!ConfigHandler.GENERAL.DamageBlocks.get()) {
                blocks.clear();
            }
            if (!ConfigHandler.GENERAL.DamagePlayers.get()) {
                entities.clear();
            }
        }
    }
}
